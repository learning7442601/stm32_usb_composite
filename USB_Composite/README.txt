# STM32_USB_Composite

The initialization of USB Composite device uses custom (not HAL implemented) approach.
This means that we implement custom written Configuration Descriptor with selected order of
interfaces (see example).


The default HAL engine uses auto-write procedure of result Configuration.
In Init code it will looks like:


//-----------------------------------------------------------------------------
  uint8_t ep_cdc[3] = {CDC_IN_EP, CDC_OUT_EP, CDC_CMD_EP};
  uint8_t ep_msc[2] = {MSC_EPIN_ADDR, MSC_EPOUT_ADDR};
  /* Register CDC class in Composite device */
  if(USBD_RegisterClassComposite(&hUsbDeviceFS,
		  	  	  	  	  	  	  &USBD_CDC,
								  CLASS_TYPE_CDC,
								  (uint8_t*)&ep_cdc) != USBD_OK)
  {
	  Error_Handler();
  }

  if(USBD_RegisterClassComposite(&hUsbDeviceFS,
		  	  	  	  	  	  	  &USBD_MSC,
								  CLASS_TYPE_MSC,
								  (uint8_t*)&ep_msc) != USBD_OK)
  {
	  Error_Handler();
  }
  if (USBD_CMPSIT_SetClassID(&hUsbDeviceFS, CLASS_TYPE_CDC, 0) != 0xFF)
  {
	  USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
  }
  if (USBD_CMPSIT_SetClassID(&hUsbDeviceFS, CLASS_TYPE_MSC, 0) != 0xFF)
  {
	  USBD_MSC_RegisterStorage(&hUsbDeviceFS, &USBD_Storage_Interface_fops_FS);
  }
//-----------------------------------------------------------------------------