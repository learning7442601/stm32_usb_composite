/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : usb_device.c
  * @version        : v1.0_Cube
  * @brief          : This file implements the USB Device
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/

#include "usb_device.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "usbd_msc.h"
#include "usbd_storage_if.h"
#include "usbd_audio.h"
#include "usbd_audio_if.h"
#include "usbd_composite_builder.h"
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USB Device Core handle declaration. */
USBD_HandleTypeDef hUsbDeviceFS;

/*
 * -- Insert your variables declaration here --
 */
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*
 * -- Insert your external function declaration here --
 */
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/**
  * Init USB device Library, add supported class and start the library
  * @retval None
  */
void MX_USB_DEVICE_Init(void)
{
  /* USER CODE BEGIN USB_DEVICE_Init_PreTreatment */
  /* USER CODE END USB_DEVICE_Init_PreTreatment */
  /* Init Device Library, add supported class and start the library. */
  if (USBD_Init(&hUsbDeviceFS, &FS_Desc, DEVICE_FS) != USBD_OK)
  {
    Error_Handler();
  }
  HAL_PCDEx_SetRxFiFo(&hpcd_USB_OTG_FS, 0x80);
  HAL_PCDEx_SetTxFiFo(&hpcd_USB_OTG_FS, 0, 0x40);
  HAL_PCDEx_SetTxFiFo(&hpcd_USB_OTG_FS, 1, 0x40);
  HAL_PCDEx_SetTxFiFo(&hpcd_USB_OTG_FS, 3, 0x40);
#ifdef USE_USBD_COMPOSITE
#ifdef USE_AUDIO
  uint8_t ep_audio[2] = {MIDI_EP_IN, MIDI_EP_OUT};
  //--------------------------------------------------------------------
  /* Register MSC class in Composite device */
  if(USBD_RegisterClassComposite(&hUsbDeviceFS,
		  	  	  	  	  	  	  &USBD_AUDIO,
								  CLASS_TYPE_AUDIO,
								  (uint8_t*)&ep_audio) != USBD_OK)
  {
	  Error_Handler();
  }
  hUsbDeviceFS.classId--;
  USBD_AUDIO_RegisterInterface(&hUsbDeviceFS, &USBD_AUDIO_fops_FS);
  hUsbDeviceFS.classId++;
  //--------------------------------------------------------------------
#endif
#ifdef USE_CDC
  uint8_t ep_cdc[3] = {CDC_IN_EP, CDC_OUT_EP, CDC_CMD_EP};
  //--------------------------------------------------------------------
  /* Register CDC class in Composite device */
  if(USBD_RegisterClassComposite(&hUsbDeviceFS,
		  	  	  	  	  	  	  &USBD_CDC,
								  CLASS_TYPE_CDC,
								  (uint8_t*)&ep_cdc) != USBD_OK)
  {
	  Error_Handler();
  }
  hUsbDeviceFS.classId--;
  USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
  hUsbDeviceFS.classId++;
  //--------------------------------------------------------------------
#endif
#ifdef USE_MSC
  uint8_t ep_msc[2] = {MSC_EPIN_ADDR, MSC_EPOUT_ADDR};
  //--------------------------------------------------------------------
  /* Register MSC class in Composite device */
  if(USBD_RegisterClassComposite(&hUsbDeviceFS,
		  	  	  	  	  	  	  &USBD_MSC,
								  CLASS_TYPE_MSC,
								  (uint8_t*)&ep_msc) != USBD_OK)
  {
	  Error_Handler();
  }
  hUsbDeviceFS.classId--;
  USBD_MSC_RegisterStorage(&hUsbDeviceFS, &USBD_Storage_Interface_fops_FS);
  hUsbDeviceFS.classId++;
  //--------------------------------------------------------------------
#endif


#else
  if (USBD_RegisterClass(&hUsbDeviceFS, &USBD_AUDIO) != USBD_OK)
  {
    Error_Handler();
  }
  if (USBD_AUDIO_RegisterInterface(&hUsbDeviceFS, &USBD_AUDIO_fops_FS) != USBD_OK)
  {
    Error_Handler();
  }
#endif
  //--------------------------------------------------------------------
  /* Start USB Device */
  if (USBD_Start(&hUsbDeviceFS) != USBD_OK)
  {
	  Error_Handler();
  }
  //--------------------------------------------------------------------
  /* USER CODE BEGIN USB_DEVICE_Init_PostTreatment */

  /* USER CODE END USB_DEVICE_Init_PostTreatment */
}

/**
  * @}
  */

/**
  * @}
  */

